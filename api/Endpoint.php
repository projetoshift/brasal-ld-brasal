<?php
/**
 * Classe de gestão de endpoints na rest API. 
 * 
 * @author Carlos Matos | Instituto  Modal
 * @since 1.1.0
 * @package Brasal
 * 
 */
namespace Brasal\Api;
use Brasal\Core\Importer;

class Endpoint {
	/**
	 * Namespace dos endpoints da Brasal. 
	 * @author Carlos Matos | Instituto  Modal
	 * @since 1.1.0
	 * 
	 */
	private const LD_BRASAL_REST = 'brasal/v1';
	
	public function __construct() {
		
		add_action('rest_api_init', array($this, 'routes'));		
	}
	/**
	 * Cria endpoints da Brasal. 
	 * 
	 * @author Carlos Matos | Instituto  Modal
	 * @since 1.1.0
	 * @return void()
	 * 
	 */  
	public function routes() {

		register_rest_route(self::LD_BRASAL_REST, 'file_system/', array(
			'methods' => \WP_REST_Server::CREATABLE,
			'callback' => array($this, 'process_request'),
			'args' => array(
				'csv' => array(
					'description' => __('URL de origem do CSV.', LD_BRASAL_SLUG)
				)
			)
		));		
	}
	/**
	 * Processa requisição do endpoint brasal/v1/file_system
	 * 
	 * @author Carlos Matos | Instituto  Modal
	 * @since 1.1.0
	 * @param	$request	Instância da classe WP_REST_Request
	 * @return	WP_REST_Response ou WP_Error
	 */
	public function process_request(\WP_REST_Request $request) {
		$parameters = $request->get_params();
		$file = file_get_contents($parameters['csv']);
		$response = file_put_contents(ABSPATH . 'importar.csv', $file);
		if($response) {
			$feedback = __('Arquivo criado com sucesso.', LD_BRASAL_SLUG);
			return new \WP_REST_Response($feedback, 200);
			do_action('brasal_rest_response_200');
		} else {
			return new \WP_Error('bad_request', __('Houve falha na requisição', LD_BRASAL_SLUG), array('status' => 400));
			do_action('brasal_rest_response_400');
		}
		
	}
	

}
