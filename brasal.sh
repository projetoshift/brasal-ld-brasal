#!/bin/bash -i

function show_main_title {
  # Set a foreground colour using ANSI escape
  tput setaf 3

  echo "
  | __  | __  |  _  |   __|  _  |  |   
  | __ -|    -|     |__   |     |  |__ 
  |_____|__|__|__|__|_____|__|__|_____|

  "

  tput sgr0
}

function show_main_menu {
  # clear the screen
  tput clear

  # Move cursor to screen location X,Y (top left is 0,0)
  tput cup 3 0

  show_main_title

  tput cup 10 0
  # Set reverse video mode
  tput rev
  echo "O que deseja fazer?"
  tput sgr0

  tput cup 12 0
  echo "1. Adicionar o trabalho ao GIT"

  tput cup 13 0
  echo "2. Procurar e instalar pacotes do Composer"

  tput cup 14 0
  echo "3. Atualizar classes em PSR-4 e PSR-0 no autoload"
  
  tput cup 15 0
  echo "4. Instalar Node e NPM"
  
  tput cup 16 0
  echo "5. Instalar dependências via NPM"
  
  tput cup 17 0
  echo "6. Rodar tarefas do Gruntfile"

  tput cup 18 0
  echo "7. Encerrar"

  # Set bold mode
  tput bold
  tput cup 20 0

  # Get entered number in choice variable
  read -p "Entre com a opção e dê <ENTER> [1-7] " choice

  tput clear
  tput sgr0

  case "$choice" in
    "1") show_action_menu "Gerenciando tarefas do Git..." "generate_fake_emails"
         ;;
    "2") show_action_menu "Acessando o Composer..." "buy_all_guitars"
         ;;
    "3") show_action_menu "Verificando novos namespaces..." "make_coffee"
         ;;
    "4") show_action_menu "Instalando Node e NPM..." "nvm_install"
         ;;
    "5") show_action_menu "Instalando dependências via NPM..." "npm_install"
         ;;
    "6") show_action_menu "Rodando tarefas do Grunt..." "grunt_exec"
         ;;
    "7") echo "Saindo... até mais!"
         exit 0
         ;;
    *) echo "Escolha uma opção válida."
       show_main_menu
       ;;
  esac
}

function show_action_menu {
  # $1 = Action title
  # $2 = Action name
  tput clear

  tput cup 1 0

  show_main_title

  tput cup 10 0

  tput rev
  echo $1
  tput sgr0

  tput bold
  tput cup 12 0

  run_task $2
}

function run_task {
  echo -e '\033[0;33mIniciando Tarefa...\033[0m'

  tput sgr0

  task_name=$1
  

  case "$task_name" in
    "generate_fake_emails")
      # Change the line below to your action
      git add .
      echo -ne '#####                     (33%)\r'
      sleep 1
      echo -ne '#############             (66%)\r'
      sleep 1
      echo -ne '#######################   (100%)\r'
      echo -ne '\n'
	  echo -e "\033[0;33mPlugin adicionado ao Git em sua última versão.\033[0m"
      git status
	  echo -e "\033[0;33mEntre com a declaração do Commit para a versão:\033[0m"
	  read paragraph
	  echo -ne '#####                     (33%)\r'
      sleep 1
      echo -ne '#############             (66%)\r'
      sleep 1
      echo -ne '#######################   (100%)\r'
      echo -ne '\n'
	  git commit -m "$paragraph"
	  echo -e "\033[0;33mCommit armazenado.\033[0m"
      echo -e "\033[0;33mTarefas de Git realizadas.\033[0m"
	  sleep 3
	  show_main_menu
      ;;
    "buy_all_guitars")
      echo "Procurar pacotes com o termo:"
	  read pacote
	  composer search $pacote
	  echo "Insira o nome do pacote a instalar:"
	  read nomepacote
	  echo -ne '#####                     (33%)\r'
      sleep 1
      echo -ne '#############             (66%)\r'
      sleep 1
      echo -ne '#######################   (100%)\r'
      echo -ne '\n'
	  composer require $nomepacote
	
      echo "Pacotes instalados no diretório [vendor]"
	  sleep 2
	  show_main_menu
      ;;
    "make_coffee")
	  composer dump-autoload -o
      echo "Atualizando classes em PSR-4 no autoload."
	  echo -ne '#####                     (33%)\r'
      sleep 1
      echo -ne '#############             (66%)\r'
      sleep 1
      echo -ne '#######################   (100%)\r'
      echo -ne '\n'
      echo "Classes atualizadas."
	  sleep 2
	  show_main_menu
      ;;
	"nvm_install")
	  cd ~
	  echo -e "\033[0;33mBaixando arquivos do NVM...\033[0m"
	  echo -ne '#####                     (33%)\r'
      sleep 1
      echo -ne '#############             (66%)\r'
      sleep 1
      echo -ne '#######################   (100%)\r'
      echo -ne '\n'
	  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
	  echo -e "\033[0;33mAssociando padrões executáveis.\033[0m"
	  
	  source ~/.bashrc
	  echo -e "\033[0;33mVerificando versões.\033[0m"
	  nvm -v
	  echo -e "\033[0;33mInstalando Node e NPM...\033[0m"
	  echo -ne '#####                     (33%)\r'
      sleep 1
      echo -ne '#############             (66%)\r'
      sleep 1
      echo -ne '#######################   (100%)\r'
      echo -ne '\n'
	  nvm install v12.18.3
	  nvm use v12.18.3
	  echo -e "\033[0;33mHabilitando comandos em Bash...\033[0m"
	  export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion
      source ~/.bashrc
	  sleep 3
	  show_main_menu
	  ;;
	"npm_install")
	  npm install
	  echo -e "\033[0;33mInstalando dependências...\033[0m"
	  echo -ne '#####                     (33%)\r'
      sleep 1
      echo -ne '#############             (66%)\r'
      sleep 1
      echo -ne '#######################   (100%)\r'
      echo -ne '\n'
	  
	  sleep 2
	  show_main_menu
	  ;;
	"grunt_exec")
	  grunt
	  echo -e "\033[0;33mVerificando...\033[0m"
	  echo -ne '#####                     (33%)\r'
      sleep 1
      echo -ne '#############             (66%)\r'
      sleep 1
      echo -ne '#######################   (100%)\r'
      echo -ne '\n'
	  sleep 2
	  show_main_menu
	  ;;
    *) echo "Error. Action '$task_name' doesn't exist."
       exit 0
       ;;
  esac

  # Calculate current position of the cursor
  extract_current_cursor_position last_position
  last_line_number=${last_position[0]}

  tput cup $[$last_line_number + 1] 0

  tput bold
  echo '>>> Fim da Tarefa.'
}

function waiting {
printf 'Executando'
for ((i = 0; i < 3; ++i)); do
    for ((j = 0; j < 2; ++j)); do
        printf .
        sleep 3
    done

    printf '\b\b\b\b    \b\b\b\b'
done
printf '....terminado.\n'
}

#############################
#  Helpers  #
#############################

# http://askubuntu.com/questions/366103/saving-more-corsor-positions-with-tput-in-bash-terminal
function extract_current_cursor_position {
    export $1
    exec < /dev/tty
    oldstty=$(stty -g)
    stty raw -echo min 0
    echo -en "\033[6n" > /dev/tty
    IFS=';' read -r -d R -a pos
    stty $oldstty
    eval "$1[0]=$((${pos[0]:2} - 2))"
    eval "$1[1]=$((${pos[1]} - 1))"
}

# Start point
show_main_menu
