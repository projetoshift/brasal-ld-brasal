<?php 

namespace Brasal\Core;

class Admin {
    
    public function __construct() {
        
		add_filter('use_block_editor_for_post_type', array( $this, 'block_editor_removal'), 10, 2); 
        add_action('admin_enqueue_scripts', array($this, 'enqueue_styles'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts'));
		
		//Route::post('license/{key}', array($this, 'check_key'));
        
    }
	
	public function check_key($key) {
		echo $key;
	}
	
	function block_editor_removal($current_status, $post_type)
    {
        if ($post_type === 'groups') return false;
        return $current_status;
    }
    
	/**
	 * Registra arquivos CSS para o backend.
	 * @since    1.0.0
	 * 
	 * @param	$hook	String	Hook da página, em caso de restrição no registro da folha de estilo.
	 */
    public function enqueue_styles($hook) {
		
		if ( $hook != ('learndash-lms_page_brasal__options') ) {
			return;
		}
        wp_enqueue_style( 'brasal-admin', LD_BRASAL_ASSETS . 'brasal-admin.css' );
		wp_enqueue_style( 'bootstrap-admin', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' );

	}

	/**
	 * Registra arquivos Javascript para o backend.
	 * @since    1.0.0
	 * 
	 * @param	$hook	String	Hook da página, em caso de restrição no registro da biblioteca.
	 */
	public function enqueue_scripts($hook) {

		if ( $hook != ('learndash-lms_page_brasal__options') ) {
			return;
		}

		wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', null, null, true );
		wp_enqueue_script( 'bootstrap-main', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', null, null, true );

	}
}
