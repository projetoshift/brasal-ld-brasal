<?php

namespace Brasal\Core;

class Block {
	
	public function __construct() {
		
	}
	
	public function button($user) {
		    
		$user_id = $user->ID;
    	$current_user_id = get_current_user_id();
    	if ($user_id != $current_user_id):
        wp_enqueue_style('user_status_style', LD_BRASAL_ASSETS . 'desativa.css');
		
        if (current_user_can('edit_users')):
        ?>
            <table class="form-table" id="block_user">
            <tr><th>
            	<label for="user_status"><?php _e("Estado da Conta", LD_BRASAL_SLUG, 'ld-brasal'); ?></label>
            </th><td>
                <label class="tgl">
                <input type="checkbox" name="user_status" value="Inativo" id="user_status"<?php checked(get_user_meta($user_id, 'user_status', true), 'Inativo'); ?>>
				<span data-off="<?php _e("Ativo", LD_BRASAL_SLUG, 'ld-brasal'); ?>" data-on="<?php _e("Inativo", LD_BRASAL_SLUG, 'ld-brasal'); ?>"></span>
                </label>
                <span class="description"></span>
                </td></tr>
                <tr><th>
                <label for="user_status_message"><?php _e("Razão do Bloqueio", LD_BRASAL_SLUG, 'ld-brasal'); ?></label>
				</th>
                <td>
                <label class="tgl">
                <input type="text" name="user_status_message" class="regular-text" value="<?php echo get_user_meta($user_id, 'user_status_message', true) ?>">
                </label>
                </td></tr>
            </table>
        <?php
			
        endif;
    	endif;
    	return;
	}
}
