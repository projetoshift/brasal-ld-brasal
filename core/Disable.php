<?php 

namespace Brasal\Core;
use Brasal\Core\Importer;

class Disable {
	
	/**
	 * Desativa usuários a partir do conjunto de dados importados.
	 * @since    1.1.1
	 * 
	 * @return	void
	 */
    public static function cut() {
        
        $results = Importer::get();
		
		foreach($results as $args) {
			$has_user = get_user_by('login', $args['MATRICULA']);
			if($args['SITUACAO'] === 'DEMITIDO'){
				update_user_meta($has_user->ID, 'user_status', 'Inativo');
			}
		}
				
	}
}