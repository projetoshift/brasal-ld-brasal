<?php
/** 
 * Classe de importação e processamento do CSV externo.
 * 
 * @package	Brasal\Core
 * @author	Carlos Matos | Instituto Modal
 * @since	1.0.0
 * @access	public
 * @use		League\Csv\Reader
 */

namespace Brasal\Core;
use League\Csv\Reader;

class Importer {
	
	public function __construct() {
        
	}
	
	/**
	 * Acessa o URL remoto do arquivo CSV e extrai dados, convertendo o CSV para JSON array.
	 * 
	 * @since   1.0.0
	 * @return	$results	Array	Conjunto de dados dos usuários.
	 */
	public static function get() {
		
		do_action('brasal_before_get_csv');
		
		$url = cmb2_get_option(LD_BRASAL_PREFIX . '_options', LD_BRASAL_PREFIX . 'brasal_csv');
        $content = file_get_contents($url);
        $csv = Reader::createFromString($content);
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(4);
		$csv->getRecords(['MATRICULA','DATA_NASCIMENTO','NOME','APELIDO','CPF','CARGO','SITUACAO']);
        $results = $csv->jsonSerialize();

		return $results;
	}
	
		/**
	 * Acessa o URL remoto do arquivo CSV e extrai dados, convertendo o CSV para JSON array.
	 * 
	 * @since   1.0.0
	 * @return	$results	Array	Conjunto de dados dos usuários.
	 */
	public static function get_rest($url) {
		
		do_action('brasal_before_get_rest_csv');
		
        $content = file_get_contents($url);
        $csv = Reader::createFromString($content);
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(4);
		$csv->getRecords(['MATRICULA','DATA_NASCIMENTO','NOME','APELIDO','CPF','CARGO','SITUACAO']);
        $results = $csv->jsonSerialize();

		return $results;
	}
	
}
