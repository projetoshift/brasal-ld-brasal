<?php

namespace Brasal\Core;
use Brasal\Core\Importer;
use Brasal\Core\Jobs;

class Insert {
    
	/**
	 * Cria novos usuários a partir do conjunto de dados importados e, se existentes, atualiza campos.
	 * @since    1.0.0
	 * 
	 * @return	void
	 */
    public static function add() {
        
        $results = Importer::get();
		
		do_action('brasal_after_import_csv');

        foreach($results as $args) {
            $userdata = array(
                'user_login' => $args['MATRICULA'],
                'user_pass' => preg_replace("/[^0-9]/", "", $args['CPF']),
                'user_email' => preg_replace("/[^0-9]/", "", $args['CPF']) . '@brasal.com.br',
                'first_name' => $args['APELIDO'],
                'last_name' => $args['NOME'],
                'display_name' => $args['APELIDO'],
                'role' => 'subscriber'
            );
			
			$userupdate = array(
                'user_email' => preg_replace("/[^0-9]/", "", $args['CPF']) . '@brasal.com.br',
                'first_name' => $args['APELIDO'],
                'last_name' => $args['NOME'],
                'display_name' => $args['APELIDO'],
                'role' => 'subscriber'
            );
			
			$user_birth = preg_replace("/", "-", $args['DATA_NASCIMENTO']);
            
            $user_job = $args['CARGO'];
            $group = Jobs::find($user_job);
            $group_id = \Brasal\Learndash\Groups::get_id($group);
            			
			$has_user = get_user_by('login', $args['MATRICULA']);
            if($args['SITUACAO'] != 'DEMITIDO'){
				if(!$has_user) {
					do_action('brasal_before_insert_user');
					$user_id = wp_insert_user($userdata);
					update_user_meta($user_id, LD_BRASAL_PREFIX . '_userbirth', $user_birth);
					
					learndash_set_users_group_ids($user_id, array($group_id));
				} else {
					do_action('brasal_before_update_user');
					$push_id = array('ID' => $has_user->ID);
					$merge = array_merge($userupdate, $push_id);
					$user_id = wp_update_user( $merge );
					update_user_meta($has_user->ID, LD_BRASAL_PREFIX . '_userbirth', $user_birth);
								
					learndash_set_users_group_ids($has_user->ID, array($group_id));
				}
            }
        }
    }
}