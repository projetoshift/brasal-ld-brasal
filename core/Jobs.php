<?php 

namespace Brasal\Core;

class Jobs {
    
    public static function find($user_job) {
        
        $json = file_get_contents(LD_BRASAL_ASSETS . 'jobs.json');
        $array = json_decode($json);
        return $array->$user_job;
        
    }
}