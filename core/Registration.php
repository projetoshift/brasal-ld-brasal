<?php

namespace Brasal\Core;

class Registration {
	
	public function __construct() {
		
		add_action('user_profile_update_errors', array($this, 'without_email'), 10, 3 );
		add_action('user_new_form', array($this, 'user_new_form'), 10, 1);
		add_action('show_user_profile', array($this, 'user_new_form'), 10, 1);
		add_action('edit_user_profile', array($this, 'user_new_form'), 10, 1);
	}
	
	public function without_email(
								$errors,
								$update,
								$user) {
		$errors->remove('empty_email');
	}
	
	public function user_new_form($form_type) {
		
		?>
		<script type="text/javascript">
			jQuery('#email').closest('tr').removeClass('form-required').find('.description').remove();
			<?php if (isset($form_type) && $form_type === 'add-new-user') : ?>
				jQuery('#send_user_notification').removeAttr('checked');
			<?php endif; ?>
		</script>
		<?php
	}

}