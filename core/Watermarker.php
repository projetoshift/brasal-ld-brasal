<?php 

namespace Brasal\Core;

class Watermarker {
    
    public function __construct() {
        
		$this->includes();
		$this->pdf = new \FPDI_Protection( 'P', 'pt' );
    }
	
	/**
	 * Inclui bibliotecas PDF
	 * @return void
	 */
	public function includes() {
		
		if ( ! class_exists( 'FPDF' ) ) {
			require_once LD_BRASAL_LIB . 'fpdf/fpdf.php';
		}
		if ( ! class_exists( 'FPDI' ) ) {
			require_once LD_BRASAL_LIB . 'fpdi/fpdi.php';
		}
		if ( ! class_exists( 'FPDI_Protection' ) ) {
			require_once LD_BRASAL_LIB . 'fpdi/fpdi_protection.php';
		}
	}
	
	/**
	 * Testa arquivos na abertura
	 * @return int número de páginas do docuento, ou FALSE caso não abra
	 */
	public function try_open( $file ) {

		try {
			$pagecount = $this->pdf->setSourceFile( $file );
		} catch ( Exception $e ) {
			return false;
		}
			return $pagecount;
	}
}
