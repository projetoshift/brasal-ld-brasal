<?php

function bua_block_checkbox($user)
{
    $user_id = $user->ID;
    $current_user_id = get_current_user_id();
    if ($user_id != $current_user_id):
        wp_enqueue_style('user_status_style', LD_BRASAL_ASSETS . 'desativa.css');
        if (current_user_can('edit_users')):
            ?>
            <table class="form-table" id="block_user">
                <tr>
                    <th>
                        <label for="user_status"><?php _e("Estado da Conta", LD_BRASAL_SLUG, 'ld-brasal'); ?></label>
                    </th>
                    <td>
                        <label class="tgl">
                            <input type="checkbox" name="user_status" value="Inativo"
                                   id="user_status" <?php checked(get_user_meta($user_id, 'user_status', true), 'Inativo'); ?>>
                            <span data-off="<?php _e("Ativo", LD_BRASAL_SLUG, 'ld-brasal'); ?>"
                                  data-on="<?php _e("Inativo", LD_BRASAL_SLUG, 'ld-brasal'); ?>"></span>
                        </label>
                        <span class="description"></span>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="user_status_message"><?php _e("Razão do Bloqueio", LD_BRASAL_SLUG, 'ld-brasal'); ?></label>
                    </th>
                    <td>
                        <label class="tgl">
                            <input type="text" name="user_status_message" class="regular-text"
                                   value="<?php echo get_user_meta($user_id, 'user_status_message', true) ?>"
                        </label>
                    </td>
                </tr>
            </table>
        <?php
        endif;
    endif;
    return;
}
add_action('show_user_profile', 'bua_block_checkbox');
add_action('edit_user_profile', 'bua_block_checkbox');
//Session
function bua_destroy_user_session($user_id)
{
    $sessions = \WP_Session_Tokens::get_instance($user_id);
    $sessions->destroy_all();
}
//Save User Status
function bua_save_user_status($user_id)
{
    if (current_user_can('edit_users')) :
        update_user_meta($user_id, 'user_status', $_POST['user_status']);
        update_user_meta($user_id, 'user_status_message', $_POST['user_status_message']);
        bua_destroy_user_session($user_id);
    endif;
    return;
}
add_action('personal_options_update', 'bua_save_user_status');
add_action('edit_user_profile_update', 'bua_save_user_status');
//Login Error
function bua_login_authenticate($user, $username)
{
    $userinfo = get_user_by('login', $username);
    if (!$userinfo) {
        return $user;
    } elseif (get_user_meta($userinfo->ID, 'user_status', true) === 'Inativo') {
        $user_message = get_user_meta($userinfo->ID, 'user_status_message', true);
        $default_message = __('Sua conta está desativada.', LD_BRASAL_SLUG, 'ld-brasal');
        $message = !empty($user_message) ? $user_message : $default_message;
        $error = new \WP_Error();
        $error->add('account_disabled', $message);
        return $error;
    }
    return $user;
}
add_filter('authenticate', 'bua_login_authenticate', 99, 2);
//Show User Status Columns
function bua_user_status_column($column)
{
    $column['user_status'] = __('Status', LD_BRASAL_SLUG, 'ld-brasal');
    $column['user_status_reasen'] = __('Razão do Bloqueio', LD_BRASAL_SLUG, 'ld-brasal');
    return $column;
}
add_filter('manage_users_columns', 'bua_user_status_column');
function bua_show_user_status($value, $column, $userid)
{
    wp_enqueue_style('user_status_style', LD_BRASAL_ASSETS . 'desativa.css');
    $active = __('Ativo', LD_BRASAL_SLUG, 'ld-brasal');
    $blocked = __('Inativo', LD_BRASAL_SLUG, 'ld-brasal');
    $user_status = get_user_meta($userid, 'user_status', true);
    $user_status_message = get_user_meta($userid, 'user_status_message', true);
    if ('user_status' == $column) {
        if ($user_status === 'Inativo') {
            return "<span class='user-status-deactive'>" . $blocked . "</span>";
        } else {
            return "<span class='user-status-active'>" . $active . "</span>";
        }
    }
    if ('user_status_reasen' == $column) {
        if ($user_status == 'Inativo') {
            return "<div>" . $user_status_message . "</div>";
        }
    }
    return $value;
}
add_action('manage_users_custom_column', 'bua_show_user_status', 10, 3);