<?php
/**
 * Plugin Name:     Brasal - Modificações e Implementações
 * Description:     Implementações e customizações ao sistema do Learndash e ao Wordpress Core específicas para a Brasal Refrigerantes.
 * Author:          Instituto Modal
 * Author URI:      https://modal.org.br
 * Text Domain:     ld-brasal
 * Domain Path:     /languages
 * Version:         1.1.0
 *
 * @package         Brasal
 * @version			1.1.0
 * @author			Carlos Matos | Instituto Modal
 */

if( ! defined( 'ABSPATH' ) ) exit; // Evita acessos diretos

define('LD_BRASAL_VERSION', '1.1.0');
define('LD_BRASAL_SLUG', 'ld-brasal');
define('LD_BRASAL_PREFIX', 'brasal_');
define('LD_BRASAL_ASSETS', plugin_dir_url( __FILE__ ) . 'assets/');
define('LD_BRASAL_DATA', plugin_dir_path( __FILE__ ) . 'assets/');
define('LD_BRASAL_LIB', plugin_dir_path( __FILE__ ) . 'lib/');
define('LD_BRASAL_TEMP', plugin_dir_path( __FILE__ ) . 'temp/');
define('LD_BRASAL_UPDATER', plugin_dir_path( __FILE__ ) . 'updater/plugin-update-checker/');

use Brasal\Models\User;
use Brasal\Models\Options;
use Brasal\Models\Groups;
use Brasal\Core\Importer;
use Brasal\Core\Registration;
use Brasal\Core\Admin;
use Brasal\Core\Jobs;
use Brasal\Core\Insert;
use Brasal\Core\Disable;
use Brasal\Core\Watermarker;
use Brasal\Learndash\Focus;
use Brasal\Api\Endpoint;

// Chama autoload do Composer e alinha namespaces
require __DIR__ . '/vendor/autoload.php';
// Usa biblioteca Action Scheduler do WooCommerce para realizar sincronizações de usuários em background
require_once( plugin_dir_path( __FILE__ ) . '/lib/action-scheduler/action-scheduler.php' );
//Instância da classe de updater remoto
require LD_BRASAL_UPDATER . 'plugin-update-checker.php';
		$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
			'https://projetoshift.pt/server/?action=get_metadata&slug=ld-brasal',
			__FILE__,
			'ld-brasal'
		);

/**
 * Classe principal do plugin. Não pode sofrer etensões. 
 * 
 * @author Carlos Matos | Instituto  Modal
 * @since 1.0.0
 * 
 * 
 */

final class Brasal {
	
	/**
	 * @var	Object	 Instância da classe Brasal\Models\User
	 * @since	1.0.0
	 */
	protected $user;
	/**
	 * @var	Object	 Instância da classe Brasal\Core\Registration
	 * @since	1.0.0
	 */
	protected $registration;
	/**
	 * @var	Object	 Instância da classe Brasal\Models\Options
	 * @since	1.0.0
	 */
    protected $options;
	/**
	 * @var	Object	 Instância da classe Brasal\Core\Admin
	 * @since	1.0.0
	 */
    protected $admin;
	/**
	 * @var	Object	 Instância da classe Brasal\Learndash\Focus
	 * @since	1.0.0
	 */
    protected $focus;
	/**
	 * @var	Object	 Instância da classe Brasal\Models\Groups
	 * @since	1.0.0
	 */
    protected $groups;
	/**
	 * @var	Object	 Instância da classe Brasal\Core\Watermarker
	 * @since	1.0.0
	 */
    protected $water;
	/**
	 * @var	Object	 Instância da classe Brasal\Api\Endpoint
	 * @since	1.0.0
	 */
    protected $endpoint;
	
	/**
	 * Função construtora da classe principal.
	 * Instancia objetos do plugin.
	 * 
	 * @since	1.0.0
	 * @return	void
	 */
	public function __construct() {
		
		$this->user = new User();
		$this->registration = new Registration();
        $this->options = new Options();
        $this->admin = new Admin();
		$this->focus = new Focus();
		$this->groups = new Groups();
		$this->water = new Watermarker();
		$this->endpoint = new Endpoint();
					
	}
	
}

$GLOBALS['brasal'] = new Brasal();

/**
 * Cronjob de atualização dos usuários. 
 * Utiliza biblioteca Action Scheduler
 * 
 * @author Carlos Matos | Instituto  Modal
 * @since 1.1.0
 * 
 * 
 */
add_action('init', 'cronjobs');
function cronjobs() {
	if ( false === as_next_scheduled_action( 'brasal_cron' ) ) {
		$frequency = get_option(LD_BRASAL_PREFIX . '_options');
		as_schedule_recurring_action( strtotime('tomorrow'), $frequency['brasal_brasal_cron'], 'brasal_cron' );
	}
	if ( false === as_next_scheduled_action( 'brasal_disable' ) ) {
		$frequency = get_option(LD_BRASAL_PREFIX . '_options');
		as_schedule_recurring_action( strtotime('tomorrow'), $frequency['brasal_brasal_cron'], 'brasal_disable' );
	}
}

/**
 * Chama e isntancia classe e função estática de inserção de usuários. 
 * 
 * @author Carlos Matos | Instituto  Modal
 * @since 1.0.0
 * 
 * 
 */
function options_call() {
	Insert::add();
}
add_action('brasal_cron', 'options_call');

function disabling_call() {
	Disable::cut();
}
add_action('brasal_disable', 'disabling_call');

