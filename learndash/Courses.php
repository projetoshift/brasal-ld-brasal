<?php
/** 
 * Classe com funções relacionadas aos cursos do Learndash
 * 
 * @package	Brasal\Learndash
 * @author	Carlos Matos | Instituto Modal
 * @since	1.0.0
 * @access	public
 */

namespace Brasal\Learndash;

class Courses {
	
	public function __construct() {}
	
	public static function get_ids(Array $course_titles) {
		
		$course_ids = array();
		foreach($course_titles as $title) {
			$course = get_page_by_title($title, OBJECT, 'sfwd-courses');
			$course_ids[] = $course->ID;
		}
		
		return $course_ids;
	}
}