<?php
/** 
 * Modificações ao Focus Mode do Learndash.
 * 
 * @package	Brasal\Learndash
 * @author	Carlos Matos | Instituto Modal
 * @since	1.0.0
 * @access	public
 */

namespace Brasal\Learndash;

class Focus {
	
	public function __construct() {
		
		add_filter('learndash_focus_mode_collapse_sidebar', array($this, 'collapsed_init'));
		add_filter( 'learndash_show_next_link', array($this, 'next_link'), 10, 3 );
	}
	
	public function collapsed_init() {
		return true;
	}
	
	public function next_link($show_next_link = true, $user_id = 0, $post_id = 0) {
		$post_type = get_post_type( $post_id );
    	if ( $post_type == 'sfwd-lessons')
    		$show_next_link = false;
	}
}
