<?php
/** 
 * Classe com funções relacionadas aos grupos do Learndash
 * 
 * @package	Brasal\Learndash
 * @author	Carlos Matos | Instituto Modal
 * @since	1.0.0
 * @access	public
 */

namespace Brasal\Learndash;

class Groups {
	
	public function __construct() {}
	
	/**
	 * Find a group ID by supplying its title
	 * 
	 * @since	1.0.0
	 * @returns		int		$group_id	ID do grupo solicitado.
	 */
	public static function get_id($title) {
		
		$group = get_page_by_title($title, OBJECT, 'groups');
		$group_id = $group->ID;
		
		return $group_id;
	}
	
	public static function set_group($user_id, $group_id, $exists = true) {
		
		$groups = array($group_id);
		learndash_set_users_group_ids($user_id, $groups, $exists);
	}
	
	public static function get_groups($user_id) {
		
		$groups_ids = learndash_get_users_group_ids($user_id);
		$groups = array();
		foreach($groups_ids as $group_id) {
			$groups[] = get_the_title($group_id);
		}
		
		return $groups;
	}
}