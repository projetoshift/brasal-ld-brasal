<?php

namespace Brasal\Models;
use Brasal\Models\Metabox;

class Options extends Metabox {
	
	public function __construct() {
        parent::__construct(
            array(
				'id' => LD_BRASAL_PREFIX . '_brasal_page',
                'title' => __('Brasal Refrigerantes', LD_BRASAL_SLUG, 'ld-brasal'),
                'description' => __('Brasal Refrigerantes - opções relacionadas às extensões.', LD_BRASAL_SLUG, 'ld-brasal'),
				'object_types' => array( 'options-page' ),
                'classes' => array('col-md-12', 'form-group'),
                'cmb_styles' => false,
				'option_key' =>LD_BRASAL_PREFIX . '_options',
                'parent_slug' => 'learndash-lms',
                'capability' => 'manage_options',
                'display_cb' => array($this, 'page_output'),
                'save_button' => esc_html__( 'Salvar Configurações', LD_BRASAL_SLUG, 'ld-brasal' ),
			),
            array(
                array(
                    'name' => __( '<strong>URL do Arquivo</strong>', LD_BRASAL_SLUG, 'ld-brasal' ),
                    'description' => __( 'Link ou caminho até o arquivo CSV de dados de usuários.', LD_BRASAL_SLUG, 'ld-brasal' ),
                    'id'   => LD_BRASAL_PREFIX . 'brasal_csv',
                    'type' => 'text_url',
                    'classes' => 'col-md-6',
                    'protocols' => array( 'http', 'https', 'ftp', 'ftps'),
                ),				
                array(
                    'name' => __( '<strong>Programação</strong>', LD_BRASAL_SLUG, 'ld-brasal' ),
                    'description' => __( 'Periodicidade de atualização.', LD_BRASAL_SLUG, 'ld-brasal' ),
                    'id'   => LD_BRASAL_PREFIX . 'brasal_cron',
                    'type' => 'select',
                    'classes' => 'col-md-6',
                    'show_option_none' => true,
                    'options'          => array(
                        'hourly' => __( 'A cada hora', LD_BRASAL_SLUG, 'ld-brasal' ),
                        'twicedaily' => __( 'A cada 12 horas', LD_BRASAL_SLUG, 'ld-brasal' ),
                        'daily' => __( 'A cada 24 horas', LD_BRASAL_SLUG, 'ld-brasal' ),
                    ),
                ),
                array(
                    'name' => __( '<strong>Arquivo CSV de Usuários</strong>', LD_BRASAL_SLUG, 'ld-brasal' ),
                    'description' => __( 'Se não houver arquivo, o sistema tentará buscá-lo remotamente.', LD_BRASAL_SLUG, 'ld-brasal' ),
                    'id'   => LD_BRASAL_PREFIX . 'brasal_user_file',
                    'type' => 'file',
                    'classes' => 'col-md-6',
                    'options' => array(
                        'url' => false,
                    ),
                    'text'    => array(
                        'add_upload_file_text' => __('CSV para Anexar', LD_BRASAL_SLUG, 'ld-brasal' )
                    ),
                )
            )
        );
    }
    
    public function page_output( $hookup ) {
	// Output custom markup for the options-page.
        ?>
        <style>
        #wpfooter {display: none;}
        </style>
        <div class="wrap container-fluid">
            <div style="height: 20px;"></div>
                <div class="row">
                <div class="col-md-8">
                    <h4 class="display-4" style="font-size: 2em;">Brasal Refrigerantes</h4>
                </div>
                <div class="col-md-4">
                    <p class="description text-right">
                    Opções e configurações relacionadas às extensões e implementações ao sistema do Learndash e Wordpress.
                    </p>
                </div>
                    <hr>
                </div>

            <div style="height: 20px;"></div>
            
            
            <div class="col-md-12">

            <div class="alert alert-success" role="alert">

                <h4 class="alert-heading">Bem-vindo!</h4>
                <p>Este painel concentra todas as opções e configurações que se aplicam às customizações realizadas pelo <strong>Instituto Modal</strong> à plataforma do Wordpress e Learndash. Alterações que não possam ser realizadas por aqui necessitam de apoio ou suporte técnico.</p>
                <hr>
                <p class="mb-0">No caso de dúvidas, entre em contato conosco.</p>
            </div>
            <div style="height: 20px;"></div>
            
            <div class="wrap cmb2-options-page option-<?php echo $hookup->option_key; ?>">

                <form class="cmb-form" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="POST" id="<?php echo $hookup->cmb->cmb_id; ?>" enctype="multipart/form-data" encoding="multipart/form-data">
                    <input type="hidden" name="action" value="<?php echo esc_attr( $hookup->option_key ); ?>">
                    <?php $hookup->options_page_metabox(); ?>
                    <nav class="navbar fixed-bottom navbar-light bg-light">
		                <ul class="navbar-nav ml-auto">
            		    <li class="nav-item">
                        <?php submit_button( esc_attr( $hookup->cmb->prop( 'save_button' ) ), 'primary', 'submit-cmb' ); ?>
                        </li>
  		                </ul>
  
	               </nav>
                </form>
            </div>
        </div>
            
        <script>
        // Wordpress button style override
        var element = document.getElementById("submit-cmb"); 
        element.classList.remove("button","button-primary");
        element.classList.add("btn","btn-success");
        </script>
        <?php
    }
	
}