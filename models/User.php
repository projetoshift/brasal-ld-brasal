<?php

namespace Brasal\Models;
use Brasal\Models\Metabox;

class User extends Metabox {
	
	public function __construct() {
		parent::__construct(
			array(
				'id' => LD_BRASAL_PREFIX . '_user',
				'title' => 'Dados Brasal Refrigerantes',
				'object_types' => 'user',
				'context' => 'normal',
				'priority' => 'high',
				'show_names' => true,
				'show_in_rest' => \WP_REST_Server::READABLE,
			),
			array(
				array(
					'name' => __( 'ID do Funcionário', LD_BRASAL_SLUG, 'ld-brasal' ),
					'desc' => __( 'Número de registro de controle do RH e sistema', LD_BRASAL_SLUG, 'ld-brasal' ),
					'id'   => LD_BRASAL_PREFIX . '_userid',
					'type' => 'text',
					'column' => array(
						'position' => 4,
						'name'     => 'ID do Funcionário',
					),
				),
				array(
					'name' => __( 'Data de Nascimento', LD_BRASAL_SLUG, 'ld-brasal' ),
					'id'   => LD_BRASAL_PREFIX . '_userbirth',
					'type' => 'text_date',
					// 'timezone_meta_key' => 'wiki_test_timezone',
					'date_format' => 'd-m-Y',
				)
			)
		);
	}
	
}