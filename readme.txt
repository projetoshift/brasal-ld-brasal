=== Brasal - Modificações e Implementações ===
Contributors: (this should be a list of wordpress.org userid's)
Tags: comments, spam
Requires at least: 4.5
Tested up to: 5.5.1
Requires PHP: 7.1
Stable tag: 0.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Plugin de customização e implementação de recursos adicionais da Brasal Refrigerantes.

== Description ==

Recursos adicionais ao sistema do Learndash, core do Wordpress e outros plugins e estruturas da plataforma. Painel de configurações simples à disposição como submenu do Learndash LMS.

Este plugin implementa:

*   Novos campos no perfil dos usuários
*   Possibilidade de inserção de usuários sem e-mail
*   Campo para bloquear usuários, configurando-os como INATIVOS
*   Rotina de importação de novos usuários ou de atualização de usuários existentes via CSV remoto, indicado na URL das configurações.
*   Barra lateral da área de estudos inicialmente recuada
*   Sincronização dos usuários com os seus respectivos grupos
*   Bases para calendarização de módulos através dos grupos no Learndash
*	API endpoint para upload do arquivo CSV de sincronização de usuários

== Planejamento ==

= 1.1.0 =
* Sistema de calendarização de módulos por grupos habilitado
* Endpoint e namespace da API `brasal/v1/file_system` para gerenciar uploads remotos

== Changelog ==

= 1.0.1 =
* Sistema de atualização e busca de updates em servidor remoto

= 1.0.0 =
* Primeira versão estável do plugin