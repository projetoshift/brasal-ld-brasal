<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Rareloop\\WordPress\\Router\\' => array($vendorDir . '/rareloop/wp-router/src'),
    'Rareloop\\Router\\' => array($vendorDir . '/rareloop/router/src'),
    'League\\Csv\\' => array($vendorDir . '/league/csv/src'),
    'Brasal\\Updater\\' => array($baseDir . '/updater'),
    'Brasal\\Models\\' => array($baseDir . '/models'),
    'Brasal\\Learndash\\' => array($baseDir . '/learndash'),
    'Brasal\\Elementor\\' => array($baseDir . '/elementor'),
    'Brasal\\Core\\' => array($baseDir . '/core'),
    'Brasal\\Api\\' => array($baseDir . '/api'),
);
